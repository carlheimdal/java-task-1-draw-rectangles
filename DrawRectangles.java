import java.util.Scanner;

public class DrawRectangles{
  int width;
  int height;
  String sign;
  String line = "";

  DrawRectangles(int width, int height, String sign){
    this.width = width;
    this.height = height;
    this.sign = sign;
  }

  private String rectanglesBorders(int i, int j){
    // Returns this.sign if there should be a sign and a space otherwise
   if(
     i == 0
     || i == this.height - 1
     || j == 0
     || j == this.width - 1
     || ((i == 2 || i == this.height - 3) && (j > 1 && j < this.width - 2))
     || ((i > 2 && i < this.height - 2) && (j == 2 || j == this.width - 3))
   ){
     return this.sign;
   } else {
     return " ";
   }
  }
  private void Draw(){
    // Iterate each column and row. Print rows (line) one by one.
    for(int i = 0; i < this.height; i++){
      this.line = "";
      for(int j = 0;  j < this.width; j++){
        this.line += rectanglesBorders(i, j);
      }
      System.out.println(this.line);
    }
  }

  public static void main(String[] args){
    // Get user input
    Scanner scan = new Scanner(System.in);
    System.out.println("Welcome to the rectangle in rectangle drawer!");
    System.out.println("Choose rectangle width: ");
    int width = scan.nextInt();
    System.out.println("Choose reactangle height: ");
    int height = scan.nextInt();
    System.out.println("Choose the character to print the rectangles with: ");
    String sign = scan.next();
    scan.close();

    DrawRectangles draw = new DrawRectangles(width, height, sign);
    draw.Draw();
  }
}